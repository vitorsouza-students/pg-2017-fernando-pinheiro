# README #

Projeto de Graduação de Fernando Guimarães Pinheiro: _Aplicação do método FrameWeb no desenvolvimento de um sistema de informação usando um framework PHP e um framework JavaScript_.

### Resumo ###

FrameWeb é um método para o projeto de Sistemas de Informação Web que utilizam em sua arquitetura alguns _frameworks_ comuns no desenvolvimento Orientado a Objetos, como controlador frontal, mapeamento objeto/relacional e injeção de dependência. Criado em 2007 e recentemente revisado dentro da metodologia de Desenvolvimento Orientado a Modelos, o FrameWeb vem sendo usado em diferentes projetos e arquiteturas/plataformas para verificar a generalidade de suas propostas.

Um desses projetos é o Sistema de Controle de Afastamento de Professores (SCAP), sistema Web desenvolvido em dois outros Projetos de Graduação na plataforma Java, com _frameworks_ controladores frontais diferentes. Partindo do levantamento de requisitos já realizado nestes projetos passados, este trabalho tem como objetivo reimplementar este sistema em outra plataforma de desenvolvimento, combinando as linguagens de programação PHP e JavaScript, de modo a avaliar se o método FrameWeb se adequa a este novo contexto.

Dado o objetivo de verificar o quão aderente o método FrameWeb se apresenta quando as tecnologias que foram utilizadas para o desenvolvimento da aplicação mudam, utilizou-se como metodologia a Engenharia Reversa, desenvolvendo em primeiro lugar a aplicação para, em momento posterior, documentá-la com os modelos de projeto prescritos pelo FrameWeb.