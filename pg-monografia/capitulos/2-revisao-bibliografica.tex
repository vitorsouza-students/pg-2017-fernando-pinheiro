\chapter{Engenharia Web, \textit{frameworks} e FrameWeb}
\label{sec--revisao-bibliografica}

Neste capítulo são apresentados todos os importantes conceitos usados no projeto. Inicialmente falamos sobre a Engenharia Web e como as aplicações voltadas para a Internet são construídas. Após isso são apresentados os \textit{frameworks} utilizados no projeto. Por fim, descrevemos o FrameWeb, um método voltado para o desenvolvimento de sistemas de informação Web baseados em \textit{frameworks}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Engenharia Web
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Engenharia Web}
\label{sec--revisao-bibliografica--engenharia-web}

A Internet se tornou uma tecnologia indispensável para os negócios, comércio, comunicação, educação, engenharia, entretenimento, medicina e várias outras áreas. Podemos ver sua importância e o seu impacto em nossas vidas: a usamos para comprar produtos, conhecer pessoas, consumir conteúdos, expressar nossa opinião e muito mais. O veículo usado para que tudo isso seja possível são as aplicações baseadas na Web (\textit{Web-based Applications}), também conhecida como WebApps~\cite{pressman2009web}.

Nos primórdios da Internet, por volta de 1990 a 1995, os \textit{websites} não passavam de vários arquivos de hipertexto vinculado uns ao outros. Com o passar do tempo e com o surgimentos de novas tecnologias e ferramentas de desenvolvimentos houve um aumento da capacidade de desenvolvimento, tanto para o \textit{client-side} (lado do cliente) quanto para o \textit{server-side} (lado do servidor) desses \textit{websites}~\cite{pressman2009web}.

Quando falamos de WebApps alguns atributos estão presentes na maioria delas. Entre outros vários temos: intensidade de rede, concorrência, carga imprevisível, sensibilidade a performance, alta disponibilidade, orientação aos dados, imediatividade, segurança.

O \textit{HyperText Transfer Protocol} (HTTP) é o ingrediente mais básico pelo qual a Web foi fundada. É um protocolo de aplicação cliente-servidor que define um formato padrão para que recursos sejam requisitados na Web. Foi inventado no começo da década de 1990 visando a criação de sistema distribuídos de hipermídia, usando o protocolo TCP/IP~\cite{casteleyn2009}.

Para cada requisição HTTP feita é obtida uma resposta. Quando um usuário entra com uma URI na barra de endereço de um navegador, esse navegador se encarrega de fazer a requisição requerendo um determinado recurso de um determinado servidor e obtendo a resposta desse servidor.

Na requisição é necessário informar o seguinte:
\begin{itemize}
	\item \textit{Method}: o método a ser utilizado. Os mais utilizados e que são implementados nos principais navegadores do mercado são o \textit{GET} e o \textit{POST}, porém temos outros como \textit{PUT}, \textit{PATCH}, \textit{DELETE}, \textit{OPTIONS}, etc;
	\item URI: identificador do recurso requerido;
	\item \textit{Header}: (opicional) cabeçalho com instruções para o servidor;
	\item \textit{Body}: (opicional) corpo da requisição, podem se usado como parâmetros.
\end{itemize}

Já na resposta da requisição são retornados:
\begin{itemize}
	\item \textit{Status Code}: código da resposta. É um número de três dígitos onde o primeiro define a classe do código. Podem ser 1xx para informação; 2xx para sucesso; 3xx para redirecionamento; 4xx para erros do cliente; e 5xx para erros do servidor;
	\item \textit{Header}: cabeçalho com informações adicionais passada pelo servidor;
	\item \textit{Body}: (opicional) corpo da resposta, o conteúdo em si.
\end{itemize}

%%% Arquiteturas Multicamadas de Aplicações Web %%%
\subsection{Arquiteturas Multicamadas de Aplicações Web}
\label{sec--revisao-bibliografica--engenharia-web--arquitetura}

Aplicações Web de grande escala, tais como portais e aplicações de comércio eletrônico, tipicamente estão expostas a um grande número de requisições concorrentes e devem exibir um alto nível de disponibilidade. Para tal, são necessárias arquiteturas de software modulares e multicamadas, nas quais os diferentes componentes possam ser facilmente replicados para aumentar o desempenho e evitar pontos de falha~\cite{casteleyn2009}.

Arquiteturas de software de aplicações Web dessa natureza são normalmente
organizadas em três camadas de software~\cite{casteleyn2009}:
\begin{itemize}
	\item Camada de Apresentação: responsável por processar as requisições vindas do cliente e construir as páginas HTML. É desenvolvida usando as extensões de servidores Web, as quais são capazes de construir dinamicamente as páginas HTML a serem enviadas como resposta para o cliente. Essas páginas são geradas tomando por base os dados produzidos pela execução de componentes de negócio, que estão na camada abaixo.
	\item Camada de Lógica de Negócio: responsável por executar componentes que realizam a lógica de negócio da aplicação. Para tal, comunica-se com a camada de gerência de recursos para acessar bases de dados persistentes, sistemas legados ou para invocar serviços externos.
	\item Camada de Gerência de Recursos: representa o conjunto de serviços oferecidos por diferentes sistemas, tais como aqueles suportando o acesso a bases de dados, a outros sistemas ou, de maneira geral, a serviços Web externos.
\end{itemize}

A Figura~\ref{fig-revisao-bibliografica-ambiente-execucao} ilustra como é o ambiente de execução comumente utilizado na construção de WebApps.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.9\textwidth]{figuras/fig-revisao-bibliografica-ambiente-execucao}
	\caption{Ambiente de Execução para Arquitetura Multicamadas (adaptado de ~\cite{casteleyn2009}).}
	\label{fig-revisao-bibliografica-ambiente-execucao}
\end{figure}



%%% Uma abordagem diferente %%%
\subsection{Uma abordagem diferente}
\label{sec--revisao-bibliografica--engenharia-web--abordagem-diferente}

Dois fatores são importantes de serem analisados ao perceber que o ambiente de execução apresentado na Figura~\ref{fig-revisao-bibliografica-ambiente-execucao} tem sofrido algumas alterações. O primeiro é o aumento da necessidade de comunicação entre sistema independentes.

Cada vez mais precisamos comunicar sistemas com outros sistemas terceiros, a construção e o consumo de APIs tem-se tornado uma prática comum no desenvolvimento de WebApps. Há inclusive quem defenda que a primeira coisa que deva ser desenvolvida na aplicação é a API, o movimento API-First\footnote{\url{http://www.api-first.com/}} tem chamado a atenção de vários desenvolvedores.

O outro fator é o constante avanço das tecnologias voltadas para a construção de RIAs (\textit{Rich Internet Applications}), caracterizadas por suportar comportamentos sofisticados na interface (e.g. \textit{drag\&drop}); mecanismos para minimizar a transferência de dados fazendo com que a interface faça o gerenciamento das interações e apresentações; armazenando dados tanto no lado do cliente quanto no lado do servidor; e execução também feita tanto no lado do cliente quanto no lado do servidor~\cite{casteleyn2009}. Hoje podemos ter interfaces construídas somente com o uso de JavaScript.\footnote{\url{https://www.javascript.com/}} Além disso essa linguagem de programação está entre a mais popular no mundo segundo~\citeonline{weinberger2017}.

Com isso em vista podemos juntar esses dois pontos, ou seja, para a construção de uma WebApp podemos dividi-la em duas partes, uma responsável pela API e outra responsável pela RIA. A Figura~\ref{fig-revisao-bibliografica-ambiente-execucao-diferente} demonstra como seria o ambiente de execução dessa nova abordagem.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.9\textwidth]{figuras/fig-revisao-bibliografica-ambiente-execucao-diferente}
	\caption{Ambiente de execução multicamadas vista de uma nova abordagem.}
	\label{fig-revisao-bibliografica-ambiente-execucao-diferente}
\end{figure}

A primeira requisição HTTP é feita no lado do cliente, que irá retornar os recursos necessários para executar a RIA no navegador do cliente, que pode ser um desktop, tablet, smartphone ou qualquer dispositivo que rode um navegador.

Após a RIA estar rodando no navegador do usuário, uma outra aplicação é requerida: aquela destinada para a lógica de negócio, chamada de ``lado do servidor''. Requisições HTTP são feitas para o servidor Web, que por sua vez faz uma requisição para o motor de script, que executa as rotinas existentes no servidor de aplicação. O servidor de aplicação por sua vez pode fazer várias consultas para outros servidores como de banco de dados, e-mail e gerenciamento de arquivos. O retorno da requisição HTTP sempre será um JSON (\textit{JavaScript Object Notation}).

JSON é uma formatação leve de troca de dados. Para seres humanos, é fácil de ler e escrever. Para máquinas, é fácil de interpretar e gerar. Está baseado em um subconjunto da linguagem de programação JavaScript mas mantém seu formato texto completamente independente de linguagem~\cite{json}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \textit{frameworks}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Frameworks}
\label{sec--revisao-bibliografica--frameworks}

Para a construção de WebApps uma prática muito comum é a utilização de \textit{framework}s. Um \textit{framework} em desenvolvimento de software, é uma abstração que une códigos comuns entre vários projetos de software provendo uma funcionalidade genérica. O objetivo ao se usá-los é não perder tempo fazendo atividades repetitivas e que são comuns a vários sistemas.

%%% MVC e Laravel %%%
\subsection{MVC e Laravel}
\label{sec--revisao-bibliografica--frameworks--mvc-laravel}

MVC, abreviação de \textit{Model-View-Controller}~\cite{gamma1994}, é uma arquitetura de software desenvolvida para o Smalltalk-80\texttrademark. Tem uma aceitação alta e atualmente é uma das arquiteturas mais utilizadas para a construção de sistemas Web.

As três camadas são usadas para separar a aplicação. A camada de interação do usuário (\textit{View}), a camada de manipulação dos dados (\textit{Model}) e a camada de controle (\textit{Controller}).

Toda a lógica de negócio da aplicação fica no \textit{Model}. Já a \textit{View} é a camada de interação com o usuário. Ela apenas faz a exibição dos dados, sendo ela por meio de um HTML, XML, JSON, entre outros. O \textit{Controller} é usado para intermediar o \textit{Model} e a \textit{View}. A Figura~\ref{fig-revisao-bibliografica-frameworks-mvc} demonstra a comunicação entre esses componentes do padrão.

\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.5]{figuras/fig-revisao-bibliografica-frameworks-mvc}
	\caption{Padrão MVC~\cite{programmingHelp2013}.}
	\label{fig-revisao-bibliografica-frameworks-mvc}
\end{figure}

O Lavarel\footnote{\url{https://laravel.com}} é um \textit{framework} MVC feito em PHP bastante expressivo e tem uma sintaxe bem atraente, valorizando a simplicidade e legibilidade. Com uma documentação bem completa e fácil compreensão tem grande aderência de programadores iniciais e além disso a curva de aprendizado baixa.

O \textit{framework} em si é a composição dezenas de pacotes, que não necessariamente são feitos pela mesma pessoa. A ideia por trás disso é reaproveitar o máximo possível de código e se concentrar no que é realmente relevante para o desenvolvedor. Esses pacotes são gerenciados pelo \textit{Composer}, que é um gerenciador de pacotes PHP.

O Laravel é feito por programadores para programadores e há um vasto ecossistema em torno do \textit{framework}. Várias bibliotecas (pacotes) são feitas sob medida para funcionarem com o Laravel, a comunidade é bem presente e ativa e tudo é \textit{open source}. Por esses motivos não é de se estranhar que seja um dos mais populares \textit{frameworks} de PHP.

Alguns de seus pacotes possuem destaque, como por exemplo o \textit{Eloquent},\footnote{\url{https://laravel.com/docs/master/eloquent}} que é o pacote ORM utilizado por padrão. Por ter o \textit{Eloquent} como classe herdada, os modelos da aplicação já tem, sem nenhuma configuração, uma série de métodos disponíveis para interação com o banco de dados. Se usadas as convenções propostas pelo \textit{framework} ganha-se muita agilidade nesse mapeamento, mas como é prática no Laravel, o programador tem a opção de fazer isso por conta própria.

Funcionando como se fosse um sistema de versão para o banco de dados, o Laravel tem uma funcionalidade interessante chamada de \textit{Migrations}.\footnote{\url{https://laravel.com/docs/master/migrations}} As \textit{Migrations} são classes que possuem instruções relativas ao banco de dados, como a criação de tabelas, alterações de colunas, etc., e com um ordem de prioridade. Sendo assim é garantido que os todos os comandos referente ao esquema do banco de dados sejam executados na mesma ordem, independente de onde esse sistema esteja rodando.

Outros pacotes conhecidos do \textit{framework} são o Homestead\footnote{\url{https://laravel.com/docs/master/homestead}} e Valet\footnote{\url{https://laravel.com/docs/master/valet}} para criação de todo o ambiente de desenvolvimento (sistema operacional e todos os programas e serviços configurados); Cashier\footnote{\url{https://laravel.com/docs/master/billing}} para interfaces com \textit{gateway} de pagamento; Horizon\footnote{\url{https://laravel.com/docs/master/horizon}} para gerenciamento de filas; entre outros.

%%% MVVM, Vue.js e  %%%
\subsection{MVVM, Vue.js e Vuex}
\label{sec--revisao-bibliografica--frameworks--mvvm-vuejs-vuex}

O padrão MVVM (\textit{Model}, \textit{View}, \textit{View-Model}) foi revelado pela primeira vez por John Gossman,  arquiteto do WPF\footnote{\url{https://msdn.microsoft.com/pt-br/library/cc564903.aspx}} e Silverlight\footnote{\url{https://www.microsoft.com/silverlight}} pela Microsoft\footnote{\url{https://www.microsoft.com}} e é uma variação do padrão PM (\textit{Presentation Model})~\cite{fowler2004}. O interessante desse padrão é que uma abstração da \textit{view} é criada, onde compartamentos e estados são separados da apresentação~\cite{smith2009}. É importante notar que ambos MVVM e PM são derivados do padrão MVC.

Nesse padrão o \textit{Model} se refere ao nível do domínio, representando o estado ou ao acesso aos dados, similar ao domínio apresentado no padrão MVC. Em seguida temos a \textit{View}, que assim como o padrão MVC, é responsável pela apresentação, tudo que aparece na tela do usuário. Por fim, o \textit{ViewModel}, atua entre o \textit{Model} e a \textit{View} e é responsável pela lógica de negócio da \textit{View}. Normalmente interege invocando métodos do \textit{Model} e provendo dados para a \textit{View}. A diferença do \textit{ViewModel} para o \textit{Controller} da arquitetura MVC reside no fato de que o \textit{ViewModel} não é separado da apresentação e, além disso, suporta o fluxo de dados nos dois sentidos (\textit{two-way data binding}) facilitando a propagação das alterações, entre \textit{ViewModel} e \textit{View}. A Figura~\ref{fig-revisao-bibliografica-frameworks-mvvm} demonstra a comunicação entre esses compomentes do padrão.

\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.75]{figuras/fig-revisao-bibliografica-frameworks-mvvm}
	\caption{Padrão MVVM.}
	\label{fig-revisao-bibliografica-frameworks-mvvm}
\end{figure}

Após a criação desse padrão alguns \textit{frameworks} foram criados o levando em consideração. Um desses é o Vue.js,\footnote{\url{https://vuejs.org/}} um \textit{framework} feito em JavaScript que tem com ideia separar uma coisa grande em várias pequenas, que são chamadas de componentes. A partir de um componente principal são incluídos vários outros componentes, que por sua vez incluem outros componentes, formando uma árvore de componentes. Cada componente desses possui seus próprias variáveis, métodos, eventos e funções de controle relacionados ao clico de vida (iniciação, criação, montagem, atualização, destruição, etc.).

Para que um componente possa acessar os dados dos outros componentes, prática que é muito normal dentro da abordagem, foi utilizado o Vuex,\footnote{\url{https://vuex.vuejs.org}} que é uma biblioteca de gerenciamento de estado feito para o Vue.js. O \textit{State} (estado) nada mais é do que as variáveis existentes na aplicação com os seus respectivos valores. Tendo em vista que eventos de mouse e teclado são disparados o tempo todo e que esses eventos podem invocar métodos do componente em questão, convenciona-se que o \textit{State} só pode ser alterado por meio de \textit{Actions} (ações), que são despachados e executados como se estivessem em uma fila, assim sabemos exatamente qual é o \textit{State} da aplicação antes e depois de cada evento disparado. O Vuex é inspirado no Flux\footnote{\url{https://facebook.github.io/flux}} e no Redux.\footnote{\url{https://redux.js.org}}

Uma das várias possibilidades na utilização desse padrão é na criação de SPAs (\textit{Single Page Applications}). Uma SPA é uma aplicação Web que interege com o usuário reescrevendo dinamicamente a página atual sem a necessidade do carregamento inteiro de novas páginas. Todo o código necessário para a execução dessa aplicação é carregado uma única vez, na primeira requisição. Outras requisições --- assíncronas --- podem ser feitas de acordo com a interação do usuário, são chamadas de requisições AJAX (\textit{Asynchronous JavaScript and XML}).

Outra possibilidade são as PWAs (\textit{Progressive Web Apps}). PWAs são comumente SPAs e são construídas de uma maneira que se assemelham muito com os aplicativos para dispositivos móveis. Os principais motivadores por trás dos PWAs são confiabilidade, pois mesmo em situações adversas de conectividade após o carregamento inicial não há perda de usabilidade; rapidez, com animações fluidas e respostas instantâneas às interações; e engajamento, dado que maneira do usuário usar a PWA vai ser parecida com a que ele já usa em todos os demais aplicativos no seu dispositivo.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FrameWeb
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{FrameWeb}
\label{sec--revisao-bibliografica--frameweb}

O FrameWeb é um método de projeto para construção de sistemas de informação Web (Web Information Systems – WISs) baseado em \textit{frameworks}~\cite{souza2007frameweb}. O método propõe uma arquitetura básica que muito se aproxima da implementação do sistema e é usada durante a construção de uma aplicação.

Como é um método utilizado na fase de projeto, o FrameWeb não descreve um processo de software completo. Sugere-se que as fases apresentadas na Figura~\ref{fig-revisao-bibliografica-frameweb-fases} para o processo de desenvolvimento.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.75\textwidth]{figuras/fig-revisao-bibliografica-frameweb-fases}
	\caption{Um processo de desenvolvimento de software simples sugerido por FrameWeb~\cite{souza2007frameweb}.}
	\label{fig-revisao-bibliografica-frameweb-fases}
\end{figure}

FrameWeb define uma arquitetura lógica padrão para WISs baseada no padrão arquitetônico Service Layer (Camada de Serviço), proposto por Randy Stafford em~\cite{fowler2002}. Como mostra a Figura 4.6, o sistema é dividido em três grandes camadas: lógica de apresentação, lógica de negócio e lógica de acesso a dados.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.75\textwidth]{figuras/fig-revisao-bibliografica-frameweb-arquitetura}
	\caption{Arquitetura padrão para WIS baseada no padrão arquitetônico \textit{Service Layer}~\cite{fowler2002}.}
	\label{fig-revisao-bibliografica-frameweb-arquitetura}
\end{figure}

A camada de apresentação é responsável pelas interfaces gráficas e estão presentes nela os pacotes de Visão e Controle. 
Implementada na segunda camada está a lógica de negócio, onde estão presentes dois pacotes, o de Domínio e o de Aplicação. Já na terceira camada temos a pacote de Persistência.

O FrameWeb define uma linguagem de modelagem baseada na UML. São quatro modelos distintos que possuem como função auxiliar os programadores na fase de desenvolvimento. Os modelos são:

\begin{itemize}
	\item Modelo de Entidades: representa os objetos de domínio do problema e seu mapeamento para a persistência em banco de dados relacional. A partir dele são implementadas as classes do pacote de Domínio na atividade de implementação~\cite{souza2007frameweb}.
	\item Modelo de Persistência: representa as classes DAO~\cite{alur-et-al:book03} existentes, responsáveis pela persistência das instâncias das classes de domínio. Esse diagrama guia a construção das classes DAO, que pertencem ao pacote Persistência~\cite{souza2007frameweb}.
	\item Modelo de Navegação: representa os diferentes componentes que formam a camada de Lógica de Apresentação, como páginas Web, formulários HTML e classes de ação. Esse modelo é utilizado pelos desenvolvedores para guiar a codificação das classes e componentes dos pacotes Visão e Controle~\cite{souza2007frameweb}.
	\item Modelo de Aplicação: representa as classes de serviço, que são responsáveis pela codificação dos casos de uso, e suas dependências. Esse diagrama é utilizado para guiar a implementação das classes do pacote Aplicação e a configuração das dependências entre os pacotes Controle, Aplicação e Persistência~\cite{souza2007frameweb}.
\end{itemize}

O método FrameWeb foi aplicado no desenvolvimento deste trabalho. Todos os modelos criados serão apresentados na Seção~\ref{sec--projeto--frameweb}.